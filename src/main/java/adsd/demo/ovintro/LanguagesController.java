package adsd.demo.ovintro;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class LanguagesController implements Initializable {

    @FXML Label languagesLbl;
    @FXML Button englishBtn;
    @FXML Button dutchBtn;
    @FXML Button franceBtn;

    ResourceBundle strings = ResourceBundle.getBundle("ovapp");

    @FXML
    VBox vBox;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    public void initialize(URL url, ResourceBundle rb){
        this.resources = rb;
        this.location = url;
        vBox.setPrefWidth(1000);
        vBox.setPrefHeight(600);
        vBox.setSpacing(25);
        vBox.setAlignment(Pos.CENTER);
        franceBtn.setPrefWidth(150);
        franceBtn.setPrefHeight(75);
        englishBtn.setPrefWidth(150);
        englishBtn.setPrefHeight(75);
        dutchBtn.setPrefWidth(150);
        dutchBtn.setPrefHeight(75);
        languagesLbl.setFont(Font.font(50));
    }

    public void English(ActionEvent actionEvent) {
        Locale locale = new Locale("en");
        switchLanguage(locale);
    }

    public void Dutch(ActionEvent actionEvent) {
        Locale locale = new Locale("nl");
        switchLanguage(locale);}

    public void France(ActionEvent actionEvent) {
        Locale locale = new Locale("fr");
        switchLanguage(locale);}

    private void switchLanguage(Locale locale){
        FXMLLoader  loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("OVIntroGUI.fxml"));
        loader.setResources(ResourceBundle.getBundle("ovapp", locale));
        try {
            Parent root = loader.load();
            Stage stage = (Stage) languagesLbl.getScene().getWindow();
            stage.setScene(new Scene(root));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
