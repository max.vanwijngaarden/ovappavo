package adsd.demo.ovintro;

import java.time.LocalTime;

public class TrainData extends Data
{
    public TrainData() {
        /// === Locations A ... F ========
        var location = new Location("Aa",12,false,true);
        locationMap.put(location.getName(), location);

        location = new Location("Bb",12,true,true);
        locationMap.put(location.getName(), location);

        location = new Location("Cc",12,true,false);
        locationMap.put(location.getName(), location);

        location = new Location("Dd",12,true,true);
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Aa"), departure);
            route.addStopOver(locationMap.get("Bb"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Cc"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Dd"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
            route.write();
        }
    }
}
