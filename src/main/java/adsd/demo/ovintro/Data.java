package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class Data
{
    public final HashMap<String, Location> locationMap = new HashMap<>();
    public final SortedMap<String, Route> routeMap = new TreeMap<>();
    private ArrayList<String> transportMethods = new ArrayList<>();

    public ArrayList<String> getAllLocations() {
        ArrayList<String> locationMap = new ArrayList<>();
        for (var e : this.locationMap.entrySet()){
            locationMap.add(e.getValue().getName());
        }
        return locationMap;
    }

    public ArrayList<String> setAllTransportMethods(){
        transportMethods.add("Bus");
        transportMethods.add("train");
        return transportMethods;
    }

    public ArrayList<String> getAllTransportMethods(){
        return transportMethods;
    }


    public Location getLocation(String key)
    {
        return locationMap.get(key);
    }

    public ArrayList<Trip> getTrips(String key1, String key2, LocalTime timeInput) {
        Trips trips = new Trips();
        for (var e : routeMap.entrySet()) {
            var key = e.getKey();
            var route = e.getValue();
            var posA = key.indexOf(key1);
            var posB = key.indexOf(key2);

            if(posA>=0){
                if (posB > posA) {

                    var candidate = new Trip(route, getLocation(key1), getLocation(key2));
                    if(candidate.getDeparture().isAfter(timeInput)){
                        trips.addTrip(candidate);
                    }
                }
            }
        }
        return trips.getTrips();
    }
}
