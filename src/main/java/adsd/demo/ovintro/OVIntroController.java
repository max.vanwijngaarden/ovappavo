package adsd.demo.ovintro;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.converter.LocalTimeStringConverter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class OVIntroController
{
   Data data = new Data();
   BusData busData = new BusData();
   TrainData trainData = new TrainData();

   @FXML private ComboBox<String> comboTransport;
   @FXML private ComboBox<String> comboA;
   @FXML private ComboBox<String> comboB;
   @FXML private TextArea         textArea;
   //@FXML private TextArea         TextAreaServices;
   @FXML private Spinner timeSpinner;
   @FXML private ListView<Trip> listView;
   @FXML Button buttonPlanMyTripBtn;


   //   OvFIETS
   @FXML ImageView imageViewBycicle;
   @FXML Label OvFiets;
   @FXML Label OvFietsNumbers;

   //OvWc
   @FXML  ImageView imageViewWc;
   @FXML Label OvWc;
   @FXML Label OvWcLocation;

   //OvKiosk
   @FXML ImageView imageViewKiosk;
   @FXML Label OvKiosk;
   @FXML Label OvKioskLocation;

   @FXML Label titelServices;

   ResourceBundle strings = ResourceBundle.getBundle("ovapp");
   private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
   public String getKey1() {return comboA.getValue(); }
   public String getKey2() {return comboB.getValue(); }

   SpinnerValueFactory value = new SpinnerValueFactory<LocalTime>() {

      {
         setConverter(new LocalTimeStringConverter(formatter,null));
      }

      @Override
      public void decrement(int steps) {
         if (getValue() == null)
            setValue(LocalTime.now());
         else {
            LocalTime time = (LocalTime) getValue();
            setValue(time.minusMinutes(steps));
         }
      }

      @Override
      public void increment(int steps) {
         if (this.getValue() == null)
            setValue(LocalTime.now());
         else {
            LocalTime time = (LocalTime) getValue();
            setValue(time.plusMinutes(steps));
         }
      }
   };

      @FXML
   public void onComboA()
   {
      System.out.println( "OVIntroController.onComboA" );
   }

   @FXML
   public void onComboB()
   {
      System.out.println( "OVIntroController.onComboB" );
   }


////////////////////////////////////////////////
////////////////////////////////////////////////

   public void onPlanMyTrip() {
      listView.getItems().clear();

      if (getKey1() == null && getKey2() == null);
      else if (getKey1() == null);
      else if (getKey2() == null);
      else if (getKey1() == getKey2());
      else
      {
         ObservableList<Trip> tripList = FXCollections.observableArrayList(data.getTrips(getKey1(), getKey2(), LocalTime.parse(timeSpinner.getValue().toString())));
         listView.setItems(tripList);
      }
   }

//   @FXML
//   protected void onPlanMyTrip()
//   {
//      System.out.println( "OVIntroController.onPlanMyTrip" );
//      System.out.format( "Transport: %s\n", comboTransport.getValue() );
//      System.out.format( "From:   %s\n", comboA.getValue() );
//      System.out.format( "To:     %s\n", comboB.getValue() );
//
//      String text = String.format( "%-8s %-15s\n", strings.getString("OVType"), comboTransport.getValue() );
//      text += String.format( "%-8s %-15s\n", strings.getString("From"), comboA.getValue() );
//      text += String.format( "%-8s %-15s\n", strings.getString("To"), comboB.getValue() );
//
//      textArea.setText( text );
//   }

////////////////////////////////////////////
////////////////////////////////////////////

   public void onTransport(){
      if (comboTransport.getValue() == "Bus") {
         data = busData;
      }
      else if (comboTransport.getValue() == "train") {
         data = trainData;
      }
      ObservableList<String> locations = FXCollections.observableArrayList(data.getAllLocations());
      comboA.setItems(locations);
      comboB.setItems(locations);
   }
   Time time = new Time(new CurrentTime().currentTime());
   Timeline timeline = new Timeline(
           new KeyFrame(Duration.seconds(1),
                   e -> {
                      time.oneSecondPassed();
                      timer.setText(time.getCurrentTime());
                   }));


   // Important method to initialize this Controller object!!!
   public void initialize()
   {

      System.out.println( "init TransportSelectorController ..." );

      // Initialise the combo box comboTransport with transportation types ...
      {
         data.setAllTransportMethods();
         ObservableList<String> transportMethods = FXCollections.observableArrayList(data.getAllTransportMethods());
         comboTransport.setItems(transportMethods);


         timeSpinner.setValueFactory(value);
         timeSpinner.setEditable(true);
         timeSpinner.getValueFactory().setValue(LocalTime.now());


         ObservableList<String> locations = FXCollections.observableArrayList(data.getAllLocations());
         comboA.setItems(locations);//vertrek plaats combobox
         comboB.setItems(locations);//aankomst plaats combobox

      }

      System.out.println( "init TransportSelectorController done" );
   }

   public void listView(){
      Trip selectedItem = listView.getSelectionModel().getSelectedItem();
      textArea.setText(selectedItem.writeTravelInformation());

      //Show Services
      writeServices();
   }


   //Start StationsServices
   public void writeServices(){
      imageBycicle();
      imageWc();
      imageKiosk();
      serviceTitel();
   }
   public void serviceTitel(){
      Trip selectedItem = listView.getSelectionModel().getSelectedItem();
      titelServices.setText(selectedItem.writeStationTitel());
      OvFietsNumbers.setText(String.valueOf(selectedItem.writeOvFietsNumbers()));
      OvWcLocation.setText(selectedItem.writeWc());
      OvKioskLocation.setText(selectedItem.writeKiosk());
   }
   public void imageBycicle() {
      Image image = new Image(getClass().getResourceAsStream("Ov-Bycicle-Icon.png"));
      imageViewBycicle.setImage(image);
      OvFiets.setText("Ov Fiets");
   }
   public void imageWc(){
      Image imageWc = new Image(getClass().getResourceAsStream("Ov-Wc-Icon.png"));
      imageViewWc.setImage(imageWc);
      OvWc.setText("Toilet");
   }
   public void imageKiosk(){
      Image imageKiosk = new Image(getClass().getResourceAsStream("Ov-Kiosk-Icon.png"));
      imageViewKiosk.setImage(imageKiosk);
      OvKiosk.setText("Kiosk");
   }
   //End StationsService


}
