package adsd.demo.ovintro;

import java.time.LocalTime;

import static java.time.temporal.ChronoUnit.MINUTES;

public class Trip
{
    private final Route route;
    private final LocalTime departure;
    private final LocalTime arrival;
    private final Location locationA;
    private final Location locationB;

    public Trip(Route route, Location locationA, Location locationB){
        this.route = route;
        this.locationA = locationA;
        this.locationB = locationB;
        this.departure = route.getDepartureTime(locationA);
        this.arrival   = route.getArrivalTime(locationB);
    }

    public LocalTime getDeparture() {
        return departure;
    }

    @Override
    public String toString() {
        return  locationA.getName() + " -> " + locationB.getName() + " (" + departure + " -> " + arrival + ")";
    }

    public String writeTravelInformation(){
        var duration = MINUTES.between(departure, arrival);
        String travelInformation = " Reisinfomatie: \n Route: (" + route.getRouteString() +  ")\n";

        return travelInformation;
    }


    public String writeTrip(String departureLocation, String arrivalLocation){
        // {LocationA} (vertrektijd: xx.xx) naar {LocationB} (aankomsttijd)
        String trip = departureLocation + " vertrektijd (" + departure + ") naar " +  arrivalLocation + " vertrektijd (" + arrival + ")";
        return trip;
    }


    public String writeStationTitel(){
        String titel= locationB.getName();
        return titel;
    }
    public Integer writeOvFietsNumbers(){
        Integer fietsen = locationB.getOvFiets();
        return fietsen;
    }
    public String writeKiosk(){

        if (locationB.getKiosk() == true){return  "Aanwezig"; }
        else {return  "Niet aanwezig"; }
    }
    public String writeWc(){

        if (locationB.getWc() == true){return  "Aanwezig"; }
        else {return  "Niet aanwezig"; }
    }
}
