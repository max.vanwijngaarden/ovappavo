package adsd.demo.ovintro;

import java.util.ArrayList;

public class Trips
{
    private final ArrayList<Trip> trips = new ArrayList<>();
    public ArrayList<Trip> getTrips() { return trips; }

    public void addTrip(Trip trip) { trips.add(trip); }
}
