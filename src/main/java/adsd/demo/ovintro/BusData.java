package adsd.demo.ovintro;

import java.time.LocalTime;

public class BusData extends Data
{
    public BusData() {
        /// === Locations A ... F ========
        var location = new Location("A",120,false,true);
        locationMap.put(location.getName(), location);

        location = new Location("B",140,true,false);
        locationMap.put(location.getName(), location);

        location = new Location("C",160,false,true);
        locationMap.put(location.getName(), location);

        location = new Location("D",235,false,true);
        locationMap.put(location.getName(), location);


        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("A"), departure);
            route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("D"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
            route.write();
        }
    }
}
