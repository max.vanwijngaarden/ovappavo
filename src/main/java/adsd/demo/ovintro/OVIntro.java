package adsd.demo.ovintro;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class OVIntro extends Application
{
   @Override
   public void start( Stage stage ) throws IOException
   {
      //Force anti-aliasing in javafx fonts must come before loading fxml file
      System.setProperty("prism.lcdtext", "false");

      FXMLLoader fxmlLoader = new FXMLLoader( OVIntro.class.getResource( "OVIntroGUI.fxml" ) );
      fxmlLoader.setResources(ResourceBundle.getBundle("ovapp"));
      Parent root = fxmlLoader.load();
      Scene      scene      = new Scene( root, 1200, 600 );
      scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
      stage.setTitle(ResourceBundle.getBundle("ovapp").getString("title"));
      stage.setScene( scene );
      stage.show();

   }

    public static void main(String[] args) {
        Application.launch();
    }
}
