package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.ArrayList;

public class Route
{
    public final ArrayList<StopOver> stopOvers = new ArrayList<>();

    public Route(Location beginLocation, LocalTime departure ){

        var stopover = new StopOver( beginLocation.getName(), null, departure );
        stopOvers.add( stopover );
    }

    public LocalTime getDepartureTime(){
        return stopOvers.get(0).getDeparture();
    }
    public LocalTime getDepartureTime(Location locationA)
    {
        for (var e: stopOvers)
        {
            if (e.getName().equals(locationA.getName()))
            {
                return e.getDeparture();
            }
        }
        return null;
    }

    public LocalTime getArrivalTime() {
        return stopOvers.get(stopOvers.size() -1).getArrival();
    }
    public LocalTime getArrivalTime(Location locationB) { return getArrivalTime();}

    public void addStopOver( Location loc, LocalTime arrival, LocalTime departure )
    {
        var stopover = new StopOver( loc.getName(), arrival, departure );
        stopOvers.add( stopover );
    }

    public void addEndPoint( Location loc, LocalTime arrival )
    {
        var stopover = new StopOver( loc.getName(), arrival, null );
        stopOvers.add( stopover );
    }

    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
    public String getKey()
    {
        String key = getRouteString();

        key += ":";
        key += stopOvers.get( 0 ).getDeparture();
        return key;

    }

    public void write()
    {
        var first = stopOvers.get( 0 );
        var last  = stopOvers.get( stopOvers.size() - 1 );
        System.out.format( "Route: %s, dep. %s at %s; arr. %s at %s\n", getKey(), first.getName(), first.getDeparture(), last.getName(), last.getArrival());
    }

    public StopOver getStopOver( String key )
    {
        for (var stop : stopOvers){
            if (stop.getName().equals( key ))
            {
                return stop;
            }
        }
        return null;
    }

    public String getRouteString()
    {
        String key = stopOvers.get( 0 ).getName();
        for (int i = 1; i < stopOvers.size(); i++)
        {
            key += "-";
            key += stopOvers.get( i ).getName();
        }

        return key;

    }
}
