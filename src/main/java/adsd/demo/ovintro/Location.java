package adsd.demo.ovintro;

public class Location
{
    private final String name;
    private final int ovFiets;
    private final boolean kiosk;
    private final boolean wc;


    Location( String name, int ovFiets,boolean kiosk,boolean wc )
    {
        this.name = name;
        this.ovFiets = ovFiets;
        this.kiosk = kiosk;
        this.wc = wc;
    }

    public String getName()
    {
        return name;
    }

    public int getOvFiets() {return ovFiets;}

    public boolean getKiosk() {return kiosk;}
    public boolean getWc() {return wc;}

}
