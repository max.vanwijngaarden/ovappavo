module com.example.ovappavo {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.ovappavo to javafx.fxml;
    exports com.example.ovappavo;
}